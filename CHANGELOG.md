# Changelog


## [0.11.1] - 2021-01-29

- fix max min trend length
- usage to stdout


## [0.11.0] - 2021-01-25

- ability to manually select trend type
- ability to set plot font size (thanks Patrick Thomas)
- ability to set black-on-white style during operation (thanks Patrick
  Thomas)


## [0.10.1] - 2020-12-22

- fix indicator of missing data


## [0.10.0] - 2020-12-22

- separate crosshair tab, with position readback
- set limits on time axis


## [0.9.0] - 2020-12-10

- online second trends
- better time axis tick labels
- new tab for manually specifying time window 
- simplified export functionality from within scope
  - export plot scent to PNG, SVG, PDF
  - export data to HDF5, MAT
  - export layout to template YAML
- ability to drop crosshair at point
- support for channel units in y-axis label
