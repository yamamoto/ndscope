stages:
  - dist
  - test
  - build
  - docs
  - deploy

include:
  - project: computing/gitlab-ci-templates
    file:
      # https://computing.docs.ligo.org/gitlab-ci-templates/conda/
      - conda.yml
      # https://computing.docs.ligo.org/gitlab-ci-templates/python/
      - python.yml


# -- dist -------------------
#
# make the distribution tarball and upload it as a job artifact
#

tarball:
  stage: dist
  extends:
    - .python:build
  image: python:3


# -- test -------------------
#
# run the tests on all supported platforms.
#
# need to use conda for nds2-client
#

.test: &test
  stage: test
  extends:
    - .conda:base
    - .python:pytest
  image: igwn/base:conda
  variables:
    GIT_STRATEGY: none
    INSTALL_TARGET: "ndscope-*.tar.*"
    COVERAGE_TARGET: "ndscope"
    PYTEST_OPTIONS: "--pyargs ndscope"
  before_script:
    - !reference [".conda:base", before_script]
    # need system packages:
    # * libgl1 is needed for Qt support in pip
    # * tzdata is needed for gpstime leap second data
    - apt-get -y update
    - DEBIAN_FRONTEND=noninteractive apt-get -y install libgl1 tzdata
    - PYTHON_VERSION="${CI_JOB_NAME##*:}"
    - conda create -n test
          python=${PYTHON_VERSION}
          setuptools-scm
          pytest-console-scripts
          cairosvg
          gpstime
          h5py
          matplotlib-base
          numpy
          pyqtgraph
          python-dateutil
          python-nds2-client
          pyyaml
          pyqt >=5
          qtpy
    - conda activate test
    - !reference [".python:pytest", before_script]
  script:
    - conda activate test
    - !reference [".python:pytest", script]
  after_script:
    - . /opt/conda/etc/profile.d/conda.sh
    - conda activate test
    - !reference [".python:pytest", after_script]

test:python:3.8:
  <<: *test

test:python:3.9:
  <<: *test

test:python:3.10:
  <<: *test

test:python:3.11:
  <<: *test


# -- build ------------------

# .build:debian: &build-debian
#   only:
#     - /^debian.*$/
#   before_script:
#     - apt-get update -yqq
#     - apt-get install -yq
#           dpkg-dev
#           devscripts
#           lintian
#   script:
#     - mkdir -pv dist
#     # install build dependencies
#     - mk-build-deps --tool "apt-get -y" --install --remove
#     # build packages
#     - dpkg-buildpackage -us -uc -b
#     # lint packages
#     - lintian ../ndscope_*.changes
#     # move things into dist/
#     - mv -v ../ndscope_*.deb ../gpstime_*.{buildinfo,changes} dist/
#   artifacts:
#     paths:
#       - dist/

# build:debian:buster:
#   <<: *build-debian
#   image: debian:buster

# build:debian:bullseye:
#   <<: *build-debian
#   image: debian:bullseye

# build:debian:bookworm:
#   <<: *build-debian
#   image: debian:bookworm


# -- docs -------------------

# docs:
#   stage: docs
#   except:
#     - /^debian.*$/
#   image: python
#   before_script:
#     # install docs requirements
#     - python -m pip install sphinx sphinx_rtd_theme
#     # install this package
#     - python -m pip install .
#   script:
#     - _version=$(python setup.py --version)
#     - mkdir -v docs
#     - cd docs
#     # generate docs skeleton
#     - python -m sphinx.ext.apidoc
#           --module-first
#           --separate
#           --full
#           --ext-autodoc
#           --ext-intersphinx
#           --doc-project "gpstime"
#           --doc-author "California Institute of Technology"
#           --doc-version "${_version}"
#           --output-dir .
#           ../gpstime
#     # use sphinx_rtd_theme
#     - sed -i 's/alabaster/sphinx_rtd_theme/g' conf.py
#     # run sphinx to generate docs
#     - python -m sphinx -b html . html
#   artifacts:
#     paths:
#       - docs/html

# pages:
#   stage: deploy
#   except:
#     - /^debian.*$/
#   dependencies:
#     - docs
#   only:
#     - tags
#   script:
#     - mv docs/html public
#   artifacts:
#     paths:
#       - public
